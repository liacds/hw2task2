import java.util.*;

public class Necklace {
List<Stones> necklace = new ArrayList<>();

    public int getCarats(){
        int carats = 0;
        for ( int i =0; i<necklace.size(); i++){
            carats += necklace.get(i).getCarats();
        }
        return carats;
    }
    public int getCost(){
        int cost = 0;
        for ( int i =0; i<necklace.size(); i++){
            cost += necklace.get(i).getCost();
        }
        return cost;
    }

    public void addStone(Stones stone){
    necklace.add(stone);
    }

    public void sortByCost(){
        Collections.sort(necklace);
    }
    public Stones findStonesOfTransarencyInDiaposon(int start, int end){
        for ( int i =0; i<necklace.size(); i++){
            if(necklace.get(i).getTransparency() >= start && necklace.get(i).getTransparency()<=end ){
                return necklace.get(i);
            }
        }
        return null;
    }
    @Override
    public String toString() {
        String ret = "";
        for ( int i =0; i<necklace.size(); i++){
            ret += necklace.get(i).getName() + " ";
        }
        return ret;
    }
}
