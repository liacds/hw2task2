public class Stones implements Comparable {
    private double carats;
    private double transparency;
    private String type;
    private String name;
    private int cost;
    public Stones( String name, double carats, double transparency, int cost, String type){
        this.name = name;
        this.carats = carats;
        this.transparency = transparency;
        this.cost = cost;
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public double getCarats() {
        return carats;
    }

    public double getTransparency() {
        return transparency;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }



    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(Object o) {
        o = (Stones) o;
        Integer x = ((Stones) o).getCost();
        Integer y = this.cost;
        return x.compareTo(y);
    }
}
