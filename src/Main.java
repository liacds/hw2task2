public class Main {

    public static void main(String[] args) {
	Stones stone1 = new Stones("Diamond", 2, 0, 100, "precious");
	Stones stone3 = new Stones("Jewel", 5, 4, 500, "precious");
	Stones stone2 = new Stones("Pearl", 0.3, 100, 40, "Not precious");
	Stones stone4 = new Stones("Amethist", 0.1, 5000, 10, "Not precious");
	Necklace necklace = new Necklace();
	necklace.addStone(stone2);
	necklace.addStone(stone1);
	necklace.addStone(stone3);
	necklace.addStone(stone4);
	System.out.println(necklace);
	System.out.println(necklace.findStonesOfTransarencyInDiaposon(20, 500));
	necklace.sortByCost();
	System.out.println(necklace);
    }
}
